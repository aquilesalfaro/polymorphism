//
//  MDCProfessor.m
//  polymorphismDemo
//
//  Created by Aquiles Alfaro on 9/25/17.
//  Copyright © 2017 Aquiles Alfaro. All rights reserved.
//

#import "MDCProfessor.h"
#import "person.h"
#import "MDCPerson.h"

@implementation MDCProfessor

-(void)setSpecialty:(NSString *)specialty{
    proSpecialty = specialty;
}

-(void)setDepartment:(NSString *)department{
    proDepartment = department;
}

-(id)getSpecialty{
    return proSpecialty;
}

-(id)getDepartment{
    return proDepartment;
}


-(id)initWithValue:(NSString *)name andGender:(NSString *)gender andRole:(NSString *)role andCampus:(NSString *)campus andDepartmet:(NSString *)department andSpecialty:(NSString *)specialty{
    [super setName:name];
    [super setGender:gender];
    [super setRole:role];
    [super setCampus:campus];
    [self setDepartment:department];
    [self setSpecialty:specialty];
    return self;
}

@end
