//
//  MDCPerson.h
//  polymorphismDemo
//
//  Created by Aquiles Alfaro on 9/25/17.
//  Copyright © 2017 Aquiles Alfaro. All rights reserved.
//

#import "person.h"

@interface MDCPerson : person{
    NSString *perCampus;
    NSString *perRole;
    
}

-(void)setCampus:(NSString *)campus;
-(void)setRole:(NSString *)role;
-(id)getCampus;
-(id)getRole;
-(id)initWithValue:(NSString *)campus andRole:(NSString *)role;



@end
