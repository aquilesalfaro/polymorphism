//
//  main.m
//  polymorphismDemo
//
//  Created by Aquiles Alfaro on 9/25/17.
//  Copyright © 2017 Aquiles Alfaro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "person.h"
#import "MDCPerson.h"
#import "MDCProfessor.h"
#import "MDCStudent.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSMutableArray *myPeople = [[NSMutableArray alloc]init];
        
        person* aquiles = [[MDCStudent alloc]initWithValue:@"Aquiles Alfaro" andGender:@"Male" andCampus:@"Wolfson" andRole:@"Student" andMajor:@"Computer Science" andClassification:@"Senior"];
        
        person* ivette = [[MDCProfessor alloc]initWithValue:@"Ivette Alfaro" andGender:@"Female" andRole:@"Professor" andCampus:@"Wolfson" andDepartmet:@"EnTech" andSpecialty:@"Culinary Arts"];
        
        
        person* meagan = [[MDCStudent alloc]initWithValue:@"Meagan Alfaro" andGender:@"Female" andCampus:@"Wolfson" andRole:@"Student" andMajor:@"Medicine" andClassification:@"Freshman"];

        
        [myPeople addObject:aquiles];
        [myPeople addObject:ivette];
        [myPeople addObject:meagan];
        
        
       for (person *people in myPeople){
           NSLog(@"\nMy name is %@ and I am a %@.", [people getName], [people getGender]);
           NSLog(@"I am a %@ at MDC %@ Campus.", [people getRole], [people getCampus]);
           if([people.getRole  isEqual: @"Professor"]){

               NSLog(@"I work in the %@ department and my specialty is %@.", [people getDepartment]
                     , [people getSpecialty]);
           }
           else{
               NSLog(@"My major is %@ and I am a %@ student.", [people getMajor], [people getClassification]);
           }
               
           }
        
        
        
    }
    return 0;
}
