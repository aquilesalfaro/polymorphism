//
//  person.h
//  polymorphismDemo
//
//  Created by Aquiles Alfaro on 9/25/17.
//  Copyright © 2017 Aquiles Alfaro. All rights reserved.
//

#import <Foundation/Foundation.h>
#ifdef DEBUG
#define NSLog(FORMAT, ...) fprintf(stderr,"%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);
#else
#define NSLog(...) {}
#endif

@interface person : NSObject{
    NSString *perGender;
    NSString *perName;
    
}
//@property (getter=getRole, getter=getCampus, getter=getDepartment, getter=getSpecialty, getter=getMajor, getter=getClassification )NSString* aName;

-(void)setGender:(NSString *)gender;
-(void)setName:(NSString *)name;
-(id)getGender;
-(id)getName;
-(id)initWithValue:(NSString *)gender andName:(NSString *)name;
-(id)getMajor;
-(id)getRole;
-(id)getCampus;
-(id)getDepartment;
-(id)getSpecialty;
-(id)getClassification;



@end
