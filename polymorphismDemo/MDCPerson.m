//
//  MDCPerson.m
//  polymorphismDemo
//
//  Created by Aquiles Alfaro on 9/25/17.
//  Copyright © 2017 Aquiles Alfaro. All rights reserved.
//

#import "MDCPerson.h"
#import "person.h"

@implementation MDCPerson

-(void)setRole:(NSString *)role{
    perRole = role;
}

-(void)setCampus:(NSString *)campus{
    perCampus = campus;
}

-(id)getRole{
    return perRole;
}

-(id)getCampus{
    return perCampus;
}

-(id)initWithValue:(NSString *)campus andRole:(NSString *)role{
    [self setCampus:campus];
    [self setRole:role];
    return self;
}

@end
