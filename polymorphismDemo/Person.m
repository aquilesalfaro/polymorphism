//
//  person.m
//  polymorphismDemo
//
//  Created by Aquiles Alfaro on 9/25/17.
//  Copyright © 2017 Aquiles Alfaro. All rights reserved.
//

#import "person.h"

@implementation person

-(void)setGender:(NSString *)gender{
    perGender = gender;
}

-(void)setName:(NSString *)name{
    perName = name;
}

-(id)getGender{
    return perGender;
}

-(id)getName{
    return perName;
}

-(id)initWithValue:(NSString *)gender andName:(NSString *)name{
    [self setGender:gender];
    [self setName:name];
    return self;
    
}

@end
