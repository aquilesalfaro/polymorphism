//
//  MDCProfessor.h
//  polymorphismDemo
//
//  Created by Aquiles Alfaro on 9/25/17.
//  Copyright © 2017 Aquiles Alfaro. All rights reserved.
//


#import "MDCPerson.h"

@interface MDCProfessor : MDCPerson{
    NSString *proSpecialty;
    NSString *proDepartment;

}

-(void)setDepartment:(NSString *)department;
-(void)setSpecialty:(NSString *)specialty;
-(id)getDepartment;
-(id)getSpecialty;
-(id)initWithValue:(NSString *)name andGender: (NSString *)gender andRole: (NSString *)role andCampus: (NSString *)campus andDepartmet: (NSString *)department andSpecialty: (NSString *)specialty;


@end
