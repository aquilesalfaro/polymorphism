//
//  MDCStudent.h
//  polymorphismDemo
//
//  Created by Aquiles Alfaro on 9/25/17.
//  Copyright © 2017 Aquiles Alfaro. All rights reserved.
//

#import "MDCPerson.h"

@interface MDCStudent : MDCPerson{
    NSString *stuMajor;
    NSString *stuClassification;
}

-(void)setMajor:(NSString *)major;
-(void)setClassification:(NSString *)classification;
-(id)getMajor;
-(id)getClassification;
-(id)initWithValue:(NSString *)name andGender: (NSString *)gender andCampus: (NSString *)campus andRole: (NSString *)role andMajor:(NSString *)major andClassification: (NSString *)classification;

@end
