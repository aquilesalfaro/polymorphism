//
//  MDCStudent.m
//  polymorphismDemo
//
//  Created by Aquiles Alfaro on 9/25/17.
//  Copyright © 2017 Aquiles Alfaro. All rights reserved.
//

#import "MDCStudent.h"
#import "person.h"
#import "MDCPerson.h"

@implementation MDCStudent

-(void)setMajor:(NSString *)major{
    stuMajor = major;
}

-(void)setClassification:(NSString *)classification{
    stuClassification = classification;
}

-(id)getMajor{
    return stuMajor;
}

-(id)getClassification{
    return stuClassification;
}

-(id)initWithValue:(NSString *)name andGender:(NSString *)gender andCampus:(NSString *)campus andRole:(NSString *)role andMajor:(NSString *)major andClassification:(NSString *)classification{
    [super setName:name];
    [super setGender:gender];
    [super setCampus:campus];
    [super setRole:role];
    [self setMajor:major];
    [self setClassification:classification];
    return self;
}


@end
